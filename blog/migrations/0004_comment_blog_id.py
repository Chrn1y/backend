# Generated by Django 2.0.6 on 2018-06-19 12:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='blog_id',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
